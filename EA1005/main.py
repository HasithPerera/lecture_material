#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 12:41:16 2020

@author: hasith@fos.cmb.ac.lk
"""

#!/usr/bin/python3

import docx
from docx.enum.style import WD_STYLE_TYPE



if __name__=="__main__":

    files = ['files/EA9000_Q1.docx','files/Error_no_section_break.docx']


    doc = []
    
    file = files[0]
    with open(file,'rb') as fp:
            doc = docx.Document(fp)
            
    for i,section in enumerate(doc.sections):
        print("Section no:",i)
        print("footer First-page-different:",section.different_first_page_header_footer)
    
#    print("\nStyle checking")
#    for i,style in enumerate(doc.styles):
#        print("\n",style.name)
#        try:
#            tmp_f = style.font
#            print("font face:",style.font.name)
#            print("font sz:",style.font.size) 
#            print("font_all_caps:",style.font.all_caps)
#  
#        except:
#            pass
#            
#        try:
#            print("alignment:",style.paragraph_format.alignment)
#            print("space before:",style.paragraph_format.space_before)
#            print("space after:",style.paragraph_format.space_after)
#        except:
#            pass


    for i,para in enumerate(doc.paragraphs):

        print(i,":",para.text)
        print(i,":",para.style.name)
        
        if (para.style.name=='Normal'):
            pass
            
        
        for ii in range(0,len(para.runs)):
            print(ii,"-",para.runs[ii].text)
        


##       print("pg no format:",section.footer.paragraphs[0].text)
#    for file in files:
#        print("\n",file)
#        
#        with open(file,'rb') as fp:
#            doc = docx.Document(fp)
##        paragraph_styles = [s for s in doc.styles if s.type == WD_STYLE_TYPE.PARAGRAPH]
#        print("author:", doc.core_properties.author)
##        for st in paragraph_styles:
##            print(st.name)
#            
#        for p in doc.paragraphs:
#            print(p.style.name,"-",p.text[0:20])
#            
          
        