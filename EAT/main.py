# Author : Hasith Perera (hasith@fos.cmb.ac.lk)
# basic python scripts for sending bulk SMS using Huawei e3372

import serial
import time

from serial.tools import list_ports

	
def send_at(ser,cmd):
	""" Send at command and get the immediate reply"""

	print("[cmd]{}".format(cmd))
	ser.write("{}\r\n".format(cmd).encode('utf-8'))
	data = ser.readline()
	#print(data)

def send_sms(ser,no="0776490562",text="Hi test"):
	"""Set the needed configs and send SMS"""

	send_at(ser,"AT")
	send_at(ser,"AT+CREG=2")
	send_at(ser,"AT+CMGF=1")
	send_at(ser,'AT+CMGS="{}"\r{}\x1A\r\n'.format(no,text))
	print("sms sent to - {}".format(no))



if __name__=='__main__':
	print("Test")
	dongle = []
	ports = list_ports.comports()
	for p,des,hw in ports:
		#print("{}-{}".format(p,des))
		if "HUAWEI" in des:
			dongle = p
			break
	if dongle == []:
		print("Dongle not found")
		exit()
	else:
		print("found dongle at {}".format(dongle))
		
	ser = serial.Serial(dongle,115200)
	if ser.isOpen():
		ser.close()
		ser.open()
	
	time.sleep(1)
	body = """Python can do any thing :P
Line 2 of the body
"""

	send_sms(ser,"0703538138",body)
	
	
	
	
