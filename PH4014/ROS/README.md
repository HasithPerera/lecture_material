# ROS instructions

# Installation using pc

The easiest method to run ROS in a linux environment is using a docker.The user should be added to the docker group to give the needed permissions to run

```bash
sudo apt install docker.io
sudo usermod -aG docker <user>
```
command| usage
 --- | --- |
`docker ps`| lists all the running containers
`docker container ls -a` | show all with hidden unnamed containers
`docker pull <name>`|To get docker from docker hub
`docker exec -ti <name> <command>`| to create additional session for a running container

