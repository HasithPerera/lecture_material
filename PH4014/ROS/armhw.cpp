#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sensor_msgs/JointState.h>
#include <iostream>
#include <stdio.h>
#include <errno.h>

void robotics_Callback(const sensor_msgs::JointState::ConstPtr& msg);

int main(int argc, char **argv)
{	int servo_drive;
	ros::init(argc, argv, "listener");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("joint_states", 1000, robotics_Callback);
	servo_drive=init_i2cdrive();
	test_motor(servo_drive,1,80,1000);
	//ROS_INFO("test 1");
	ros::spin();

	return 0;
}

void robotics_Callback(const sensor_msgs::JointState::ConstPtr& msg)
{

	int i=0;
	ROS_INFO("cmd: mtr:%d pos:%f ",i,msg->position[i]);
	//instruction to command each motor with radians as inputs
	
}
