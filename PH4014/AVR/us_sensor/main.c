#define F_CPU 16000000UL
#define BAUD 9600

#include <stdio.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "avr/pgmspace.h"

#include <util/setbaud.h>
#include "serial.h"

uint16_t overflw_cnt=0;

float dist=0;

void ultrasonic(void)
{

    PORTD &= !_BV(PD3); //low
    _delay_us(5);
    PORTD |= _BV(PD3);  //high
    _delay_us(10);
    PORTD &= ~_BV(PD3);	//low
}


void timer0_init(void)
{

    TCCR0B |= _BV(CS01); //CLK 2MHz
    TIMSK0 |= (1<<TOIE0);//counter overflow intrupt
    TCNT0 = 0; //reset the timer

}
void init_intrpt()
{
    EICRA|=_BV(ISC00); //int0 any logical change
    EIMSK|=_BV(INT0);
}

float cnvtdst(void)
{
    return ((overflw_cnt*255+TCNT0)*0.0085);
}



int main(void)
{


    stdout = &mystdout;

     DDRD = !_BV(PD2) | _BV(PD3) ;   //echo,trigger
 
    sei();
    init_intrpt();
    uart_init();
 
    printf("Start");

    timer0_init();


    while(1)
    {
        ultrasonic();
        _delay_ms(1000);

    }

    return 0;
}

ISR(INT0_vect)
{
    if(PIND & _BV(PD2))
    {
        TCNT0=0;
        overflw_cnt=0;
        //printf("High\n");
    }
    else
    {
        float a=cnvtdst();
        printf("ovflw:%d\t cnt:%d\n",overflw_cnt,TCNT0);
    }
}


ISR(TIMER0_OVF_vect)
{
    // keep a track of number of overflows
    overflw_cnt++;


}
