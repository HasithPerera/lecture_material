/*
void fwd(void);
void rev(void);
void left(void);
void right(void);
void time1_init(void);
void init_pin(void);
*/
int max_speed=40000;
int speed=20000;


void set_max(int a){
 max_speed=a;
}
void set_speed(int a){
 speed=a;
}

void time1_init(void)
{


    //  Set up Timer 1. Timer 1 should reset when
    //  it reaches TOP = ICR1 (WGM13:0 = 1110b). On
    //  compare match clear output, at TOP set (COM1A1:0 = 10b).

    TCCR1A = _BV(COM1A1) | !_BV(COM1A0)                 //  Both PWM outputs set at TOP,
             | _BV(COM1B1) | !_BV(COM1B0)            //    clear on compare match
             | !_BV(FOC1A) | !_BV(FOC1B)             //  PWM mode, can't force output
             | _BV(WGM11) | !_BV(WGM10);             //  Fast PWM, TOP = ICR1

    TCCR1B = !_BV(ICNC1) | !_BV(ICES1)                  //  Disable input capture noise canceler,
             //    edge select to negative.
             | _BV(WGM13) | _BV(WGM12)               //  Fast PWM, TOP = ICR1
             | !_BV(CS12) | _BV(CS11) | !_BV(CS10);   //  clk(i/o) / 8


    ICR1 = max_speed; //  PWM period ( 50 Hz)

    OCR1A = 20000;
    OCR1B = 20000;
}

void init_pin(void)
{

    DDRD   = _BV (PD4) | _BV (PD5);    // direction outputs
    DDRB   = _BV (PB1) | _BV (PB2);    // PWM outputs

    //DDRD   &=~_BV(PD6) | ~_BV(PD7);     //line fllw IR sensor inputs


}

void stop(void)
{
    OCR1A=0;
    OCR1B=0;
    PORTD &= ~_BV(PD4);
    PORTD &= ~_BV(PD5);

}

void fwd(void)
{
    OCR1A = speed;
    OCR1B = max_speed-speed;
    PORTD |= _BV(PD4);
    PORTD &= ~_BV(PD5);

}

void rev(void)
{

    OCR1B = speed;
    OCR1A = max_speed-speed;
    PORTD |= _BV(PD5);
    PORTD &= ~_BV(PD4);
}

void right(void)
{

    stop();
    OCR1B = max_speed-speed;
    PORTD |= _BV(PD4);

    OCR1A = speed;
    PORTD |= _BV(PD5);

}

void left(void)
{

    stop();
    OCR1B = speed;
    PORTD &= ~_BV(PD4);

    OCR1A = max_speed-speed;
    PORTD &= ~_BV(PD5);


}


