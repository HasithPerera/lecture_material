/*
* Atmega328p
*
* Blink the LED connected to pin PB5 
*/

#define F_CPU 16000000UL

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRB |= _BV(DDB5);	// PortB pin 5 as output

	while(1)
	{
		PORTB ^= _BV(PB5);
		_delay_ms(1000);

	}

}

