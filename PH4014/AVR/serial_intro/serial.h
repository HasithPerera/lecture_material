//stdout = &mystdout;



char command[]="          ";

void uart_init(void)
{
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */
}

void uart_putchar(unsigned char c)
{
    loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
    UDR0 = c;
}


char uart_getchar(void)
{
    loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
    return UDR0;
}


int uart_putchar_printf(char var, FILE *stream)
{
    if (var == '\n') uart_putchar('\r');
    uart_putchar(var);
    return 0;
}

/*
* process commands of the form a65536 where a means command, and the five-digit number is the value to be written to the register
*/
void uart_getcmd(void)
{
    unsigned char tmp,i;

    for(i=0; i<10; i++)
    {
        command[i] = ' ';
    }
    for(i=0; i<10; i++)
    {
        loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
        tmp=UDR0;
        if(tmp == 0x0D) break;
        command[i] = tmp;
        loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
        UDR0 = tmp;
    }
}

static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar_printf, NULL, _FDEV_SETUP_WRITE);
