# PH4014 - Introduction to Robotics - 2020

Detailed instructions about the robotic car([available here](https://docs.google.com/document/d/1rfoozfHwjbmESwd-9cg1xQQVprtqDvNGS61fIlNzOYc/edit?usp=sharing))

## AVR basics
- [x] basic
- [x] use USART with AVR
- [x] interfaceing HC04 Ultrasound distance sensor
- [ ] interfacing i2c devices
	- [ ] MPU6050
- [ ] DS18b20
- [ ] Line following

## ROS basics

ROS installation and testing using dockers and raspi


