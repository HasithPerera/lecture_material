# PH4019 - PLC Programming

Containts the designes for PLC sumulation boards

## Conveyor setup

Version 3
![PLC conveyor V3][plc_v3]{width=50%} 

## Subcomponents 
- [x] CAD design for basic structure 
- [ ] NEMA17 stepper driver unit using a nano
- [ ] Pusher unit
- [ ] Proximity Sensor moduel

[plc_v3]:./conveyor_setup/v3/V3.png
