# Lecture Material

Contains supporting material for lectures conducted/supported by Me during 2018 - 2020 @ UOC.



Subject Code | Title | Year
--- | --- | --- |
PH4019|Industrial automation| 2018 - 2020
PH4014| Introduction to robotics| 2018 - 2020
EA1004| Introduction to Computer programming | 2020 - VPL
EA3006| Embeded linux developement | 2018 -2020
MSc | Fpga and Nodemcu work | 2018 - present
PH3020| Computational Physics lab| 2020 (jupyter hub configuration)

## VPL References
- [vpl_evalscript](https://github.com/jcrodriguez-dis/moodle-mod_vpl/blob/master/jail/default_scripts/vpl_evaluate.cpp)  references on writing custon grading scripts

## Special topics
-	ROS instructions can be found under PH4014
-	AVR basics can be found under PH4014
-	ESP32 based weather station unde MSc/esp32
-	bulk SMS sending code available in **EAT**


