#include <stdio.h>

int main(){
	// Write a program to get N numbers from the user and find 
	// the minimum and the maximum in the given set of numbers
	// Assume: No user errors are occuring in the inputs
	// 	 : Inputs are floating point numbers
	// 	 : display the results only for two decimal places
	//Example program output should be as follows,
	//
	//Enter N:4
	//6.00
	//7.8906
	//6
	//5
	//Min:5.00
	//Max:7.89
	//
	int N = 0,i;
	printf("Enter N:");
	scanf("%d",&N);
	float min,max,value;
	scanf("%f",&value);
	
	max = value;
	min = value;
	
	for(i=1;i<N;i++){
		scanf("%f",&value);
		if (value>max)
			max = value;
		if (value<min)
			min = value;
	}
	printf("Min:%.2f\n",min);
	printf("Max:%.2f\n",max);

	return 0;
}
