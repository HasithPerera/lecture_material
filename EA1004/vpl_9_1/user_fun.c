//Complete the function given.
//Inputs: temperature value + unit


float convert_temp(float value,char type){
	if(type=='f')
		return (value-32)*5/9.00;
	else
		return 9.00*value/5.00 +32;
}

void print_conversion(float value,char type){
	char to_type;
	if (type=='c')
		to_type ='f';
	else if(type=='f')
		to_type = 'c';
	else{
		printf("Type Error\n");
		exit(0);
	}
	printf("%.2f %c > %.2f %c\n",value,type,convert_temp(value,type),to_type);
}
