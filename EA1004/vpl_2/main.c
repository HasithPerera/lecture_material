#include <stdio.h>

int main(){
    
    //Write a program to indicate if a user entered number is odd or even
    // print "odd" for an odd number
    // print "even" for an even number
    // print "error" for any error
    // assumptions: user input can be any number
    
    //Note: Donot Print unwanted stuff in your final submission
    char a[20];
    scanf("%s",a);
	
	int num1;
	float numf;
	
	sscanf(a,"%f",&numf);
	sscanf(a,"%d",&num1);

	if(num1!=numf){
    	printf("error");
    	return 0;
	}

    if (num1%2==0){
        printf("even\n");

    }else{
        printf("odd\n");
    }
    
    return 0;
}
