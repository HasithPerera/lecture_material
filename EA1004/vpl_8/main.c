#include <stdio.h>

int main(){

	// Write a program to count the number of Males and Females
	// Program structure should be as follows
	// Example program outputs
	// 
	// Enter no of students:5
	// >M
	// >F
	// >F
	// >M
	// >M
	// Males:3
	// Females:2
	//
	//If there is incorrect out put skip that and retry 
	//DONOT Print any error messages
	//consider valid inputs to be any of the following f/F/M/m
	
	int total = 0;
	int males = 0;
	int females = 0;
	char sex ;
	
	printf("Enter no of students:");
	scanf("%d",&total);
	for(int i =0;i<total;i++){
		while(1){
			printf(">");
			scanf(" %c",&sex);
			if (sex == 'M'|| sex =='m'){
				males ++;
				break;
			}else if(sex == 'F'|| sex == 'f'){
				females ++;
				//printf("Female\n");
				break;
			}
		}
		//printf("%d\n",i);
	}
	printf("Males:%d\n",males);
	printf("Females:%d\n",females);

	return 0;
}
