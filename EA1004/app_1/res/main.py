import numpy as np



if __name__=='__main__':
    
    n = [10,20,30,50]
    for i in n:
        file_name = "file_{}.csv".format(i)
        with open(file_name,'w') as fp:
            for num in range(0,i):
                fp.write("{}\n".format(np.random.rand()*100-50))
        print("File done")

