//You are only allowd to upload this file 
//For grading it will be included to a custom main function.
//hence DO NOT change the function prototype in any way.

void sort(float *values,int length){
    //The values pointer will point to the array containing unsorted values
    //lenght will contain the size of the array
    
	int i = 0;
	int j;
	float tmp;
	for(i=0;i<length;i++){
		for(j=0;j<length-(i+1);j++){
			if (*(values+j)>*(values+j+1)){
				tmp=*(values+j+1);
				*(values+j+1)=*(values+j);
				*(values+j)=tmp;
			}
		}
	}
}
