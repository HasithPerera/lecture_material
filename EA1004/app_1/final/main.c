#include <stdio.h>
#include <string.h>
#include "bubble_sort.c"

// You are suppose to use the function defined in Activity 1 - Part 2
// Get the file name as a command-line argument
// if the file is unavailable print "Error file <filename> not found" and exit.
// Read the file and indicate the number of elements in the file by printing "line count:<number>"
// Sort the array and save the content in a new file "sorted_<filename>" print the values in the upto 5 decimal places.
// one number per line is expected in the file

int read_float(char *file_name,float *val){
	FILE *fp;

	fp = fopen(file_name,"r");
	if (fp==NULL){
		return -1;
	}
	int line_count = 0;
	float tmp;
	while(fscanf(fp,"%f\n",&tmp)!=EOF){
		if (val!=NULL){
			*(val+line_count) = tmp;
		}
		line_count++;
	}
	return line_count;
}

void write_file(char *file_name,float *val,int n){

	char new[50];
	int i;
	strcpy(new,"sorted-");
	strcat(new,file_name);
	FILE *fp;
	
	//printf("file:%s\n",new);	
	fp = fopen(new,"w");
	for(i=0;i<n;i++){
		fprintf(fp,"%.5f\n",*(val+i));
	}
	fclose(fp);
}

void print_array(float *val,int n){

	int i;
	for(i=0;i<n;i++){
		printf("%.4f,",*(val+i));
	}
	printf("\n");
}

int main(int arg_count,char *arg_val[]){

	char file_name[50];
	if(arg_count <=1){
		printf("Missing file name\n");
		return -1;
	}else{
		
		strcpy(file_name,arg_val[1]);
	}
	
	int linecount;
	linecount = read_float(file_name,NULL);
	if (linecount==-1){
		printf("Error file %s not found\n",file_name);
		return -1;
	}
	printf("line count:%d\n",linecount);
	float values[linecount];
	read_float(file_name,values);	
	
	sort(values,linecount);
	write_file(file_name,values,linecount);
	
	return 0;   
}
