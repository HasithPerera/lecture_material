#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "bubble_sort.c"

void print_array(float *start,int N){
	int i;
	for(i=0;i<N-1;i++){
		printf("%.2f,",*(start+i));
	}
	printf("%.2f\n",*(start+i));

}



void sort_ahe(float *values,int length){
    //The values pointer will point to the array containing unsorted values
    //lenght will contain the size of the array
    int i = 0;
	int j;
	float tmp;
	for(i=0;i<length;i++){
		for(j=0;j<length-(i+1);j++){
			if (*(values+j)>*(values+j+1)){
				tmp=*(values+j+1);
				*(values+j+1)=*(values+j);
				*(values+j)=tmp;
			}
		}
	}

}

void fill_array(float *stu,float *test,int n){
    int i;
    float tmp;
    for(i=0;i<n;i++){
        tmp = (rand()*100.00/RAND_MAX)-50.00;
        *(stu+i)=tmp;
        *(test+i)=tmp;
    }
    
}

int match_array(float *stu,float *test,int n){
    int j;
    for(j=0;j<n;j++){
        if(*(stu+j)!=*(test+j))
            //printf("Mismatch\n");
            return 0;
        
    }
    //printf("Matched\n");
    return 1;
}

int main(){
    
    int len = 10;
    int j = 1;
    float grade;
    
    
    
    for(j=1;j<4;j++){
        
    	float stu_vals[len*j];
    	float test_vals[len*j];
    	
    	
        fill_array(stu_vals,test_vals,len*j);	
    	
    	sort(stu_vals,len*j);
    	sort_ahe(test_vals,len*j);
    	
    	
    	
    	
    	if(match_array(stu_vals,test_vals,len*j)){
    	    grade +=20/3.0;
    	}else{
    	    printf("\n<|--\n");
    	    printf("-Failed Tests: array length %d\n",len*j);
    	    printf("Expected:\n>");
    	    print_array(test_vals,len*j);
    	    
    	    printf("Program output\n>");
    	    print_array(stu_vals,len*j);
    	    
    	    printf("--|>\n");
    	}
    }
    printf("\nGrade :=>>%2.0f\n",grade);
	
	return 0;
}

