#include <stdio.h>

int main(){

	//Write a program to print a box pattern indicated below
	//when row = 3 and col = 6 the following patern should be printed.
	//
	//******
	//******
	//******
	//
	//Assumptions: get the number of rows,colums (intigers) from the use as user-inputs.
	//
	
	int row,col,row_max;
	scanf("%d",&row_max);
	
	printf("case = %d rows \n",row_max);
	printf("input = %d\n",row_max);
	printf("\noutput = ");
	for(row = 0;row<row_max;row++){
		for(col = 0;col<=row;col++){
			printf("x");
		}
		printf("\n");
	}
	printf("grade reduction = 25%\n");

	return 0;

}
