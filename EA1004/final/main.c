#include <stdio.h>

float calc_final_grade(int act1,int act2,int act3,int act4);
char letter_grade(float final_marks);

int main(){

	/*
	int a[4]={55,40,50,100};	
	
	for(int i=0;i<4;i++){
		printf("enter assignment %d marks:",i+1);
		scanf("%d",(a+i));
	}
	for(int i=0;i<4;i++){
		printf("Assignment %d marks:%d\n",i+1,a[i]);
	}
	
	printf("Final grade:%.2f\n",calc_final_grade(a[0],a[1],a[2],a[3]));	
	//printf("Letter_grade:%c\n",letter_grade(final_grd));
	*/
	
	FILE *fp2;
	FILE *fp;
	
	int aa[4];
	int index;

	int grade_count[5]={0,0,0,0,0};
	printf("enter file name:");
	char fn[100];

	scanf(" %s",fn);
	fp = fopen(fn,"r");
	fp2 = fopen("mark_sheet.txt","w");
		
	while(fscanf(fp,"%d,%d,%d,%d,%d\n",&index,&aa[0],&aa[1],&aa[2],&aa[3])!=EOF){
		//	printf("%d,%d,%d,%d,%d\n",index,aa[0],aa[1],aa[2],aa[3]);
		float final_grd = calc_final_grade(aa[0],aa[1],aa[2],aa[3]);
		fprintf(fp2,"%05d,%.2f,%c\n",index,final_grd,letter_grade(final_grd));
		
		switch(letter_grade(final_grd)){
			case 'A':
				grade_count[0]++;
				break;
			case 'B':
				grade_count[1]++;
				break;
			case 'C':
				grade_count[2]++;
				break;
			case 'D':
				grade_count[3]++;
				break;
			case 'F':
				grade_count[4]++;
				break;

		}
	}
	fclose(fp);
	fclose(fp2);
	char data[5]={'A','B','C','D','F'};
	for(int i=0;i<5;i++){
		printf("%c:%d\n",data[i],grade_count[i]);
	}
	

	return 0;
}

char letter_grade(float final_marks){
	if (final_marks >= 70)
			return 'A';
	else if(final_marks >=60)
			return 'B';
	
	else if(final_marks >=50)
			return 'C';

	else if(final_marks >=40)
			return 'D';
	else
			return 'F';
}
float calc_final_grade(int act1,int act2,int act3,int act4){
	if (act1 == -1)
		act1=0;

	if (act2 == -1)
		act2=0;

	if (act3 == -1)
		act3=0;

	if (act4== -1)
		act4=0;
	
	return act1*.2 + act2*.2 + act3*.2 + act4*.4;
}
