
fp = fopen('assignment_marks.csv','w');

for i=1:110
    
    avg_mark= rand()*100+20;
    
    marks = ceil(normrnd(avg_mark,5,[4,1]));
    marks(1) = ceil(normrnd(70,5,1));
    
    
    if (rand()<10/110)
        fprintf("Dropped\n");
    else
%         fprintf("Test 2\n");
       
        for j=1:4
            if (rand()<.1)
                marks(j)=-1;
            end
            if marks(j)>100
                marks(j) = 100;
            end
        end
        fprintf(fp,"%05d,%d,%d,%d,%d\n",i,marks(1),marks(2),marks(3),marks(4));
        
        
    end
    
end
% fclose(fp);