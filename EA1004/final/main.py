import os

def compare_file(user,test):
    file1 =[]
    file2 =[]
    with open(user,"r") as fp:
        file1 = fp.read()
        
    with open(test,"r") as fp2:
        file2 = fp2.read()
        
    if (file2==file1):
        return 1;
        #print("Matched")
    else:
        print("<|--");
        print("-Failed test:input {}".format(i))
        print("Expected:")
        #with open("N-{}.txt".format(N),'r') as fp:
        for line in file2.split('\n'):
            print ('>{}'.format(line))
        print("--|>")
        print("<|--");
        print("Program output:")
        for line in file1.split('\n'):
            print ('>{}'.format(line))
        print("--|>")
        
        return 0;

if __name__=='__main__':

    grade_steps = [50,50]
    file_format = 'file_{}.csv'
    file_name = [];
    test_prefix = "sorted_ahe-"
    test_state = 0
    
    grade = 0

    for i,N in enumerate([1,2]):
        
        #os.system("./ex {}".format(file_format.format(N)))
        input_file = "assignment_marks_{}.csv".format(N)
        user_file = "mark_sheet.txt"
        test_file = "ahe_{}.txt".format(N)
        os.system('echo "{}"|./ex >>tmp.log'.format(input_file))
        
        if (compare_file(user_file,test_file)):
            grade = grade + grade_steps[i]
            test_state = test_state + 1
        else:
            pass
            

    print("<|--");
    print("-Summary of tests");
    print(">+------------------------------+");
    print(">|         {}/{} PASSED           |".format(test_state,len(grade_steps)));
    print(">+------------------------------+");
    print("--|>");

    print('Grade :=>>{}'.format(grade))

