#include<stdio.h>

#define PI 3.14159

int main(){

	//Get the radius as a user input
	//print the circumference of a circle
	//print the area of a circle
	//If there are any errors print "error"
	//assumptions: the use input can be any number
	//
	//NOTE: Donot print additional things in the final submission file
	//	It will interfer with the automated grading process

	float a = 0;
	scanf("%f",&a);
	if (a<0){
		printf("error");
		return 0;
	}

	printf("%f\n",2*PI*a);
	printf("%f\n",PI*a*a);
	return 0;

}
