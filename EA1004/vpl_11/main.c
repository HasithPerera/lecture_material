#include <stdio.h>
#include <string.h>

int main(){
	
	//Get the file name from user
	//If the file can not be found print "Error <file name> not found"
	//After opening the file read all lines and print them with the line number
	//add a tab space to separate the line number and the text
	
	char file_name[50];
	printf("Enter file name:");
	scanf("%s",file_name);
	//strcpy(file_name,"in_basic_1.txt");
	FILE *fp;
	
	fp = fopen(file_name,"r");
	if (fp==NULL){
		printf("Error %s not found\n",file_name);
		return 0;
	}

	size_t line_count=0;
	char *line = NULL;
	int line_n=0;
//	char line[100];
//	while(fscanf(fp,"%[^\n]\n",line)!=EOF){
    while(getline(&line,&line_count, fp)!=-1){
		printf("%d\t%s",line_n++,line);
	}

	return 0;
}
