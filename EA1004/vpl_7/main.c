#include <stdio.h>

int main(){

	//Write a program to print the following pattern indicated below
	//when row = 4 the following patern should be printed.
	//
	//   *
	//  ***
	// *****
	//*******
	//
	//Assumptions: get the number of rows (>= 1 intigers) from the use as user-inputs.
	//
	
	int row,col,row_max;
	scanf("%d",&row_max);
	//row_max = 5;
	printf("case = %d rows \n",row_max);
	printf("input = %d\n",row_max);
	printf("\noutput = ");
	
	for(row = 0;row<row_max;row++){
		for(col= 0 ;col<row_max*2-1;col++){
			if (col >= row_max - row-1){
					if (col > row_max + row-1)
						printf(" ");
					else
						printf("x");	
				}
				
				else
				printf(" ");
		}
		printf("\n");
	}
	printf("grade reduction = 20%\n");
	return 0;
}
