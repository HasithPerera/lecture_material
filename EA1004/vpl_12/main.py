import os

def compare_file(N):
    file1 =[]
    file2 =[]
    with open("user_log.txt","r") as fp:
        file1 = fp.read()
        
    with open("N-{}.txt".format(N),"r") as fp2:
        file2 = fp2.read()
    if (file2==file1):
        return 1;
        #print("Matched")
    else:
        print("<|--");
        print("-Failed test:input {}".format(N))
        print("Expected:")
        #with open("N-{}.txt".format(N),'r') as fp:
        for line in file1.split('\n'):
            print ('>{}'.format(line))
        print("--|>")
        
        print("Program output:")
        for line in file2.split('\n'):
            print ('>{}'.format(line))
        print("--|>")
        return 0;

if __name__=='__main__':

    grade_steps = [33 , 33 ,34]
    test_state = 0
    grade = 0

    for i,N in enumerate([5,7,10]): 
        os.system("echo \"{}\"|./ex >> tmp.out".format(N))
        if (compare_file(N)):
            grade = grade + grade_steps[i]
            test_state = test_state + 1
        else:
            pass
            
    
    print("<|--");
    print("-Summary of tests");
    print(">+------------------------------+");
    print(">|         {}/{} PASSED           |".format(test_state,len(grade_steps)));
    print(">+------------------------------+");
    print("--|>");
    
    print('Grade :=>>{}'.format(grade))

