#include <stdio.h>

int main(){

        // Write a program to get an integer (N) from the user as a user-input
        // Write "This is line <i>\n" in a file name "user_log.txt"
        // It should contain N lines starting from 0

        char file_name[] = "user_log.txt";
        FILE *fp;

        int N,i;
        printf("Enter N:");
        scanf("%d",&N);

        fp = fopen(file_name,"w");
        for(i=0;i<N;i++){
                fprintf(fp,"This is line %d\n",i);
        }
        fclose(fp);

        return 0;
}
