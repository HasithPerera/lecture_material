# EA 3006 - Embeded linux lab

## Lab work
### 1-Wire
- [x] File read write
- [x] Internal core tempreture monitor and logger
- [ ] DS18B20 temp reading extraction
- [ ] Multiple sensor logger

### i2c basics
- [x] Basic mpu6050
- [x] Basic i2c display
- [ ] Temp display

## VPL activities 

These activities are to test student programs to read the DS18B20 sensor using simulated files. q2 also test data logging.

Known bugs : if student code appends the text they get 50% of the marks.fix is to open the file in write mode in the start of the program

## Exam day scripts

-	local mqtt server used
-	welcome message is sent on the topic ea3006/welcome every 20 seconds.
-	reply sent to student on **ea3006/q2/<index\>/reply** with md5 encoded text with time info when the student sends "verify" to the topic **ea3006/q2/<index\>**
-	responses collected to [here](https://docs.google.com/forms/d/e/1FAIpQLSdKuSxvGOfJUh2nigHyv2ZNRf1-VXBNuthcDhTLbY5LVunxbw/viewform)
-	stu_decode.py is written to dicode the responces in the google sheet

### ansible

This is a great tool to install and manage large networked.The `/etc/ansible/hosts` file should be created with ip groups and playbooks can be run against connected groups.Tested playbooks for installing and coping files are available exam\_day/quick_setup/ansible.

### Raspi connect to multiple wifi's

wpa\_roam should be created example file available in exam\_day/quick_setup
