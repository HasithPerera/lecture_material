import paho.mqtt.client as mqtt
import time
import glob
import os
import subprocess


def get_cmd_txt(cmd):
    '''Return command output''' 
    returned_value = subprocess.check_output(cmd,shell=True) 
    out = returned_value.decode('utf-8').split('\n')
    return out

def get_ip():
    return get_cmd_txt('/sbin/ifconfig eth0| /bin/grep "inet" ')[0].decode().split(" ")[9]


def get_temp():
    s_id = glob.glob("/sys/bus/w1/devices/28*")
    if (len(s_id)==1):
        return s_id[0]
    else:
        return "Error"

if __name__=='__main__':
    client = mqtt.Client()
    client.connect("10.21.12.93", 1883, 60)
    while(1):
        msg = get_temp()
        print(type(msg))
        client.publish("ea3006/sensor_test/{}".format(get_ip()),get_temp())
        time.sleep(60)


