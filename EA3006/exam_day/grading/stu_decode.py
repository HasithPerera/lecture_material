import base64

with open("got.csv","r") as fp:
    for line in fp.readlines():
        try:
            print(base64.urlsafe_b64decode(line.encode('utf-8')).decode())
        except:
            print("Error")
