import paho.mqtt.client as mqtt
import time
import base64


client_1 = mqtt.Client()
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("ea3006/q2/#")
    client.publish("reply","test")

def gen_reply(topic):
    rply = base64.urlsafe_b64encode("{},{}".format(topic,time.time()).encode('utf-8'))
    return rply.decode()

def on_message(client, userdata, msg):
    user_msg = msg.payload.decode('utf-8')
    if(user_msg == 'verify'):
        client.publish("{}/reply".format(msg.topic),gen_reply(msg.topic))

client_1.on_connect = on_connect
client_1.on_message = on_message

client_1.connect("localhost", 1883, 60)
client_1.loop_forever()

