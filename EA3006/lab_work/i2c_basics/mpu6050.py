#!/usr/bin/python

import smbus
import time

# Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def get_temp():
    #refer datasheet for correction to the temperature page 30 
    return read_word_2c(0x41)

def get_gyro():
    return [read_word_2c(0x43),read_word_2c(0x45),read_word_2c(0x47)]

def get_accel():
    return [read_word_2c(0x3b),read_word_2c(0x3d),read_word_2c(0x3f)]


if __name__=='__main__':
    
    
    bus = smbus.SMBus(1) 
    address = 0x68
    #address of MPU6050

    #wake the MPU6050 up 
    bus.write_byte_data(address, power_mgmt_1, 0)
    print("Getting mpu6050 readings")
    print("Add corrections to the tempareture reading using the register map document page 30")
    while(1):
        time.sleep(.1)
        print("Temp:{}".format(get_temp()))
        print("Acc:",get_accel())
        #print(get_gyro())
