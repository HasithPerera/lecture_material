#!/usr/bin/python
import i2c_LCD
import time


lcd = i2c_LCD.lcd()

def main():
    lcd.backlight(1)
    lcd.lcd_clear()
    lcd.lcd_display_string("i2c LED line 1",1)
    lcd.lcd_display_string("i2c LED line 2",2)


def show_time():
    time_str = time.strftime("%H:%M:%S",time.gmtime(time.time()))
    #lcd.lcd_clear()
    lcd.lcd_display_string(time_str,2)
    lcd.lcd_display_string("i2c LDC time",1)
    print(time_str)   

if __name__=="__main__":
    main()
    time.sleep(1)
    lcd.lcd_clear()
    while(1):
        show_time()

        time.sleep(1)

        

