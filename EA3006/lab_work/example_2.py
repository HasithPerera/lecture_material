#!/usr/bin/python
import commands as cmd
import time
def get_core_temp():
    '''Get the core temp using vcgencmd measure_temp command'''
    term_out = cmd.getoutput('vcgencmd measure_temp')
    temp = term_out.split('=')[1]
    #print(term_out)
    return temp.split("'")[0]

def log_data(file_name,data):
    with open(file_name,'a+') as fp:
        fp.write("{}\n".format(data))


if __name__=="__main__":
    #start of the program execution
    while(1):
        current_temp = get_core_temp()
        log_data("data.csv",current_temp)
        print('{},{}'.format(time.time(),current_temp))
        time.sleep(1)
