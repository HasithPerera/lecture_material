# Jupyterhub installation with Docker

The custom docker image with octave and octave kernal should be built using the following command.
	
	docker build -f Dockerfile -t ahehub .

This docker image can be used to create an fully operational jupyterhub instance. In order to run a container with the needed configuration the following command can be used.

	docker run -d --name=ahe_final --mount type=bind,source=/home/hub_users,target=/home --mount type=bind,source=/home/hub_users/config,target=/srv/jupyterhub --mount type=bind,source=/etc/letsencrypt,target=/etc/letsencrypt --restart always --network host ahe_hub

This command binds three folders to store the follwing as listed below

- /home/hub_users as the /home
- /home/hub_users/config as the /srv/jupyterhub
- /etc/letsencrypt for ssl certificate sharing.

Directly mounting the /etc/letsencrypt folder is the only configuration option available to get the certificate and the key files without coping.

## Web server configuration using dockers

SQL root password is stored in the docker compose file along with the front and backend networking
	
	docker-compose up
	docker-compose start
	docker-compose stop

Current configuration runs phpmyadmin in port 8080. 
