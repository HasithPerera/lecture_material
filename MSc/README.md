# Xilinx 14.7 ISE in linux

run the following commands

```bash
source /opt/Xilinx/14.7/ISE_DS/settings64.sh
#to run ISE
ise
```

To program using the BASIS 2

```bash 
djtgcfg prog -d Basys2 -i 0  -f and_gate.bit
```

## fpga/basis_2

This contains an implimentation of 4 decade counters in order to implement an up counter for the 4 ssd on board the basis_2 board.

- Clock source on board clock B8
- ic7447 for BCD decording
- 32 bit counter using a n-bit counter as a clock devider
- 2 custom muxs for switching


