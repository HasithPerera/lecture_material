----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:34:27 09/12/2020 
-- Design Name: 
-- Module Name:    ic_7447 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ic_7447 is
	port (bcd : IN std_logic_vector(3 downto 0);
			SSD : OUT std_logic_vector(6 downto 0)
	);
end ic_7447;


architecture Behavioral of ic_7447 is
signal tmp: std_logic_vector(6 downto 0);
begin
	SSD <= not tmp;
	with bcd select tmp <=
		"1111110" when "0000",
		"0110000" when "0001",
		"1101101" when "0010",
		"1111001" when "0011",
		"0110011" when "0100",
		"1011011" when "0101",
		"0011111" when "0110",
		"1110000" when "0111",
		"1111111" when "1000",
		"1110011" when "1001",
		"0000000" when others;
				
end Behavioral;

