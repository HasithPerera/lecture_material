----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:10:54 09/12/2020 
-- Design Name: 
-- Module Name:    n_cntr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity n_cntr is
GENERIC ( n : NATURAL := 4 );
PORT ( clock   : IN  STD_LOGIC;
       reset_n : IN  STD_LOGIC;
		 Q       : OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0) );
end n_cntr;

architecture Behavioral of n_cntr is

	SIGNAL value : STD_LOGIC_VECTOR(n-1 DOWNTO 0);
begin
	PROCESS (clock, reset_n)
	BEGIN
	  IF (reset_n = '0') THEN
		 value <= (OTHERS => '0');
	  ELSIF ((clock'EVENT) AND (clock = '1' )) THEN
		 value <= value + 1;
	  END IF;
	END PROCESS;
	Q <= value;
end Behavioral;

