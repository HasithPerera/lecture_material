----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:37:39 09/12/2020 
-- Design Name: 
-- Module Name:    ahe_and - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ahe_and is
Port (clk: in  std_logic;
		btn2: in  std_logic	;
		sw: in std_logic;
      led: out std_logic_vector(7 downto 0);
		SSD: out std_logic_vector(6 downto 0);
		seg_select: out std_logic_vector (3 downto 0)
		);
		
end ahe_and;

architecture Behavioral of ahe_and is


	
	component n_cntr is
	GENERIC ( n : NATURAL := 4 );
	PORT ( clock   : IN  STD_LOGIC;
			 reset_n : IN  STD_LOGIC;
			 Q       : OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0) );
	end component;
	
	component ic_7447 is
	port (bcd : IN std_logic_vector(3 downto 0);
			SSD : OUT std_logic_vector(6 downto 0)
		);
	end component;
	
	component dec_ahe is
	port( bits: in std_logic_vector(1 downto 0);
			seg_slct: out std_logic_vector(3 downto 0)
			);
	end component;
	
	component counter is

	port( clock:	in std_logic;
		  clear:	in std_logic;
		  count:	in std_logic;
		  clock_out: out std_logic;
		  Q:	 	out std_logic_vector(3 downto 0)
	);
	end component;
	
	
		
	component digit_mux is

		port( Q0 : in std_logic_vector(3 downto 0);
				Q1 : in std_logic_vector(3 downto 0);
				Q2 : in std_logic_vector(3 downto 0);
				Q3 : in std_logic_vector(3 downto 0);
				en : in std_logic_vector(1 downto 0);
				O  : out std_logic_vector(3 downto 0));
				
	end component;
	
	signal c_bit0,c_bit1,c_bit2,c_bit3 : std_logic;
	signal Q0,Q1,Q2,Q3 : std_logic_vector(3 downto 0);
	signal Q:std_logic_vector(31 Downto 0);
	signal cnt: std_logic_vector(3 downto 0);
	signal mux_tmp: std_logic_vector(1 downto 0);
	
begin



 mux_tmp <=Q(11)&Q(10);

 cntn1: n_cntr generic map(32) port map(clk,sw,Q);
 ic7447: ic_7447 port map(cnt,SSD);
 dec0 :  dec_ahe port map(mux_tmp,seg_select);
 cnt0: counter port map(Q(20),'0',sw,c_bit0,Q0);
 cnt1: counter port map(c_bit0,'0',sw,c_bit1,Q1);
 cnt2: counter port map(c_bit1,'0',sw,c_bit2,Q2);
 cnt3: counter port map(c_bit2,'0',sw,c_bit3,Q3);
 mux1: digit_mux port map(Q0,Q1,Q2,Q3,mux_tmp,cnt);
  
					
 led(0)<=Q(15);
 led(1)<=Q(18);
 led(2)<=Q(20);
 led(3)<=Q(25);
 led(4)<='0';
 led(5)<=clk;
 led(6)<=sw;
 led(7)<=btn2;
 
end Behavioral;

