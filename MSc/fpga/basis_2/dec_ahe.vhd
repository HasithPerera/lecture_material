----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:10:45 09/12/2020 
-- Design Name: 
-- Module Name:    dec_ahe - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity dec_ahe is
	port( bits: in std_logic_vector(1 downto 0);
			seg_slct: out std_logic_vector(3 downto 0)
			);
end dec_ahe;

architecture Behavioral of dec_ahe is

begin
	with bits select seg_slct <=
		"1110" when "00",
		"1101" when "01",
		"1011" when "10",
		"0111" when others;

end Behavioral;

