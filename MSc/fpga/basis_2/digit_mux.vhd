----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:06:37 09/12/2020 
-- Design Name: 
-- Module Name:    digit_mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity digit_mux is

	port( Q0 : in std_logic_vector(3 downto 0);
			Q1 : in std_logic_vector(3 downto 0);
			Q2 : in std_logic_vector(3 downto 0);
			Q3 : in std_logic_vector(3 downto 0);
			en : in std_logic_vector(1 downto 0);
			O  : out std_logic_vector(3 downto 0));
			
end digit_mux;

architecture Behavioral of digit_mux is
	signal tmp: std_logic_vector(3 downto 0);

begin
	with en select tmp <=
	Q3 when "00",
	Q2 when "01",
	Q1 when "10",
	Q0 when others;

	O <=tmp;
end Behavioral;

