--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:42:04 06/27/2020
-- Design Name:   
-- Module Name:   /home/hasith/Documents/Xilinx/day4/alu_tb.vhd
-- Project Name:  day4
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: simple_alu
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY alu_tb IS
END alu_tb;
 
ARCHITECTURE behavior OF alu_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	component simple_alu is
	port(   Clk : in std_logic; --clock signal
			  A,B : in signed(7 downto 0); --input operands
			  Op : in unsigned(2 downto 0); --Operation to be performed
			  R : out signed(7 downto 0)  --output of ALU
			  );
	end component;

   --Inputs
   signal Clk : std_logic := '0';
   signal A : signed(7 downto 0) := (others => '0');
   signal B : signed(7 downto 0) := (others => '0');
   signal Op : unsigned(2 downto 0) := (others => '0');

 	--Outputs
   signal R : signed(7 downto 0);

   -- Clock period definitions
   constant Clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: simple_alu PORT MAP (
          Clk => Clk,
          A => A,
          B => B,
          Op => Op,
          R => R
        );

   -- Clock process definitions
   Clk_process :process
   begin
		Clk <= '0';
		wait for Clk_period/2;
		Clk <= '1';
		wait for Clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		wait for 1 ns;
		--addition 
		A <="00000010";
		B <="00000100";
		Op <="000";
		wait for Clk_period;
		--check max positive
		A <="01111100";
		B <="00000011";
		
		wait for Clk_period;
		--check overflow
		A <="01111100";
		B <="00000100";
		
		wait for Clk_period;
		
		--substraction
		A <="00000010";
		B <="00000100";
		
		Op <="001";
		wait for Clk_period;
		-- not 
		A<= "00001111";
		Op<="010";
		
		wait for Clk_period;
		-- Nand
		--set value which can check all 4 options in a truth table in the low most bits
		A <= "00000011";
		B <= "00000101";
		Op<="011";
			
		
		wait for Clk_period;
		-- nor 
		Op<="100";
		
		
		wait for Clk_period;
		-- and 
		Op<="101";
		
		wait for Clk_period;
		-- or
		Op<="110";
		
		wait for Clk_period;
		-- xor
		Op<="111";
		
		
		
		
	
      -- hold reset state for 100 ns.
      -- wait for 100 ns;	

      -- wait for Clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
