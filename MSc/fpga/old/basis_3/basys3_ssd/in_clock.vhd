----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2019 05:18:19 PM
-- Design Name: 
-- Module Name: in_clock - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; 
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity in_clock is
 port ( clock1  : in std_logic;
        led     : out std_logic_vector (15 downto 0);
        seg     : out std_logic_vector(6 downto 0);
        an      : out std_logic_vector(3 downto 0);
        clk     : in std_logic;
        cnt     : in std_logic;
        clr     : in std_logic);

end in_clock;

architecture Behavioral of in_clock is

component ic7447 is 
port( A,B,C,D    :in std_logic;
      seg        :out std_logic_vector (6 downto 0 ));
end component;
--component counter is
--port ( clk : in std_logic;
--           count : in std_logic;
--           clear : in std_logic;
--           bit4_cnt : out std_logic_vector (3 downto 0));
--end component;

component counter is
    port ( clk : in std_logic;
           count : in std_logic;
           clear : in std_logic;
           bit4_cnt : out std_logic_vector (3 downto 0);
           over_fl : out std_logic);
end component;

component mux2_4 is port(
    a,b,e   : in std_logic;
    y       : out std_logic_vector (3 downto 0));   
end component;

signal tmp          : std_logic_vector (63 downto 0);
signal digit0,digit1,digit2,digit3,cnt4_tmp   : std_logic_vector (3 downto 0);

component mux_new is
port ( dig0,dig1,dig2,dig3 :in std_logic_vector (3 downto 0);
        a,b : in std_logic;
        display : out std_logic_vector (3 downto 0));
end component;

signal ovf0,ovf1,ovf2,ovf3,clr_sig : std_logic;
begin

process(clock1)
begin
if clock1'event and clock1='1' then
          tmp <= tmp + '1';
end if;
end process;

led(15) <=tmp(32);
led(14) <=tmp(31);
led(13) <=tmp(30);
led(12) <=tmp(29);
led(11) <=tmp(28);
led(10) <=tmp(27);
led(9) <=tmp(26);
led(8) <=tmp(25);
led(7) <=tmp(24);

--ssd:ic7447 port map(tmp(24),tmp(25),tmp(26),tmp(27),seg);
clr_sig <= ovf3 or clr;
 

--bcd decoder
ssd:ic7447 port map(cnt4_tmp(0),cnt4_tmp(1),cnt4_tmp(2),cnt4_tmp(3),seg);

cnt0:counter port map(tmp(20),cnt,clr_sig,digit0,ovf0);
cnt1:counter port map(ovf0,cnt,clr_sig,digit1,ovf1);
cnt2:counter port map(ovf1,cnt,clr_sig,digit2,ovf2);
cnt3:counter port map(ovf2,cnt,clr_sig,digit3,ovf3);

mux_digit:mux_new port map(digit0,digit1,digit2,digit3,tmp(15),tmp(14),cnt4_tmp);
--select annode
mux:mux2_4 port map(tmp(15),tmp(14),'1',an); 

led(0)<=cnt4_tmp(0);
led(1)<=cnt4_tmp(1);
led(2)<=cnt4_tmp(2);
led(3)<=cnt4_tmp(3);

end Behavioral;
