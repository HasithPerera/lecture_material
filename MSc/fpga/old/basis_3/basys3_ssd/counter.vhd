----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2019 02:28:48 PM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; 
use ieee.std_logic_unsigned.all;




entity counter is
    port ( clk : in std_logic;
           count : in std_logic;
           clear : in std_logic;
           bit4_cnt : out std_logic_vector (3 downto 0);
           over_fl : out std_logic);
end counter;

architecture Behavioral of counter is

signal tmp : std_logic_vector (3 downto 0);

begin

process(clk,count,clear)

begin
 over_fl <='0';
if clear = '1' then
    tmp <= "0000";
elsif (clk = '1' and clk'event) then
 if count = '1' then
 
 if tmp < 9 then
        tmp <= tmp +1;
 else
    over_fl <='1';
    tmp <= "0000";
    
 end if;
end if;
end if;

end process;
bit4_cnt <=tmp;

end Behavioral;
