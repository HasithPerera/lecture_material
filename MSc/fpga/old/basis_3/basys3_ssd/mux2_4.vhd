----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2019 07:44:06 PM
-- Design Name: 
-- Module Name: mux2_4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux2_4 is port(
    a,b,e   : in std_logic;
    y       : out std_logic_vector (3 downto 0));   
end mux2_4;

architecture Behavioral of mux2_4 is
signal tmp : std_logic_vector (2 downto 0);
begin
tmp <= a & b & e;

with tmp select y <=
    "1110" when "001",
    "1101" when "011",
    "1011" when "101",
    "0111" when "111",
    "1111" when others;

end Behavioral;
