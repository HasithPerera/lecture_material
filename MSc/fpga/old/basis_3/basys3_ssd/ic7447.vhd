----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2019 01:06:52 PM
-- Design Name: 
-- Module Name: ic7447 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ic7447 is port
    ( A,B,C,D    :in std_logic;
      seg :out std_logic_vector (6 downto 0 ));
end ic7447;

architecture Behavioral of ic7447 is
signal x: std_logic_vector (3 downto 0);
signal an: std_logic_vector (3 downto 0);
signal dp: std_logic;
begin
dp <='0';
an <="1100";
x<=D & C & B & A;

with x select seg <=
    "1000000" when "0000",
    "1111001" when "0001",
    "0100100" when "0010",
    "0110000" when "0011",
    
    "0011001" when "0100",
    "0010010" when "0101",
    "0000011" when "0110",
    "1111000" when "0111",
    "0000000" when "1000",
    
    "0011000" when "1001",
    "0100111" when "1010",
    "0110011" when "1011",
    "0011101" when "1100",
    "0010110" when "1101",
    
    "0000111" when "1110",
    "1111111" when "1111";

end Behavioral;
