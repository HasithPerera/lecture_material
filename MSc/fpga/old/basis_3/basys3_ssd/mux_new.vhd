----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2019 08:31:03 PM
-- Design Name: 
-- Module Name: mux_new - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_new is
port ( dig0,dig1,dig2,dig3 :in std_logic_vector (3 downto 0);
        a,b : in std_logic;
        display : out std_logic_vector (3 downto 0));
end mux_new;

architecture Behavioral of mux_new is
signal tmp : std_logic_vector (1 downto 0);
begin
tmp <= a & b;
with tmp select display <=
   dig0 when "00",
   dig1 when "01",
   dig2 when "10",
   dig3 when "11";

end Behavioral;
