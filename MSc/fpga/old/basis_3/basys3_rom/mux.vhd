----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/27/2019 03:15:01 PM
-- Design Name: 
-- Module Name: mux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux is
  Port (slect   : in std_logic;
        data    : in std_logic_vector(7 downto 0);
        out_data: out std_logic_vector(3 downto 0)
  );
end mux;

architecture Behavioral of mux is
signal out_tmp: std_logic_vector(3 downto 0);

begin

with slect select out_data <=
    data(7 downto 4) when '1',
    data(3 downto 0) when '0';

end Behavioral;
