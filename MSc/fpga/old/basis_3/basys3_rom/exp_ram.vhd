----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/27/2019 12:59:56 PM
-- Design Name: 
-- Module Name: exp_ram - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity exp_ram is
    port( btnC: in std_logic;
          sw : in std_logic_vector(15 downto 0); 
          seg : out std_logic_vector(6 downto 0);
          led : out std_logic_vector(15 downto 0); 
          an : out std_logic_vector(3 downto 0);
          clk: in std_logic
    );
--  Port ( );
--btnC => clock to ram
end exp_ram;

architecture Behavioral of exp_ram is

--component SRAM is
--generic(	width:	integer:=4;
--		depth:	integer:=4;
--		addr:	integer:=2);
--port(	Clock:		in std_logic;	
--	Enable:		in std_logic;
--	Read:		in std_logic;
--	Write:		in std_logic;
--	Read_Addr:	in std_logic_vector(addr-1 downto 0);
--	Write_Addr: 	in std_logic_vector(addr-1 downto 0); 
--	Data_in: 	in std_logic_vector(width-1 downto 0);
--	Data_out: 	out std_logic_vector(width-1 downto 0)
--);
--end component;

component ROM is
port(	Clock   : in std_logic;
	Reset	: in std_logic;	
	Enable	: in std_logic;
	Read	: in std_logic;
	Address	: in std_logic_vector(3 downto 0);
	Data_out: out std_logic_vector(7 downto 0)
);
end component;

component ic7447 is port
    ( A,B,C,D    :in std_logic;
      seg :out std_logic_vector (6 downto 0 ));
end component;

component mux is
  Port (slect   : in std_logic;
        data    : in std_logic_vector(7 downto 0);
        out_data: out std_logic_vector(3 downto 0)  
  );
end component;

component clock is
  Port (clk :in std_logic;
        cnt :out std_logic_vector(31 downto 0)  );
end component;

signal address,bcd: std_logic_vector(3 downto 0);
signal data : std_logic_vector(7 downto 0);
signal cnt_clk : std_logic_vector(31 downto 0);

begin



--led(0)<=sw(0);
--led(1)<=sw(1);
--led(2)<=sw(2);
--led(3)<=sw(3);

address <= sw(3) & sw(2) & sw(1) & sw(0);

rom0: ROM port map(
Clock=>btnC,
Reset=>sw(15),
Enable=>sw(14),
Read =>sw(13),
Address=>address,
Data_out=>data);
clck0 : clock port map(clk,cnt_clk);

mux0: mux port map(
    slect => cnt_clk(11),--  : in std_logic;
    data  => data,--  : in std_logic_vector(7 downto 0);
    out_data => bcd   -- out std_logic_vector(3 downto 0)  
);
--multiplex the annode
with cnt_clk(11) select an <=
    "1101" when '1',
    "1110" when '0';

ic0: ic7447 port map(
A=>bcd(0),
B=>bcd(1),
C=>bcd(2),
D=>bcd(3),  
seg=>seg); 
--an<="1100";


--led(7 downto 0)<=data;
--led(15)<=sw(15);
--led(14)<=sw(14);
--led(13)<=sw(13);
led(12)<=btnC;

led(7 downto 0)<=cnt_clk(31 downto 24);



end Behavioral;
