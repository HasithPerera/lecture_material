----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/13/2019 09:21:32 PM
-- Design Name: 
-- Module Name: adder1bit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity adder1bit is port
    ( a,c_old,b: in std_logic;
     s,c : out std_logic
    );
--  Port ( );
end adder1bit;

architecture Behavioral of adder1bit is
signal a_b,b_b,c_b : std_logic;

begin

a_b<=not a;
b_b<=not b;
c_b<=not c_old;
s <= (a_b and b and c_b) or (a and b_b and c_b) or (a_b and b_b and c_old) or (a and b and c_old);
-- or (a and b_b and c_b) or (a_b and b_b c_old) or (a and b and c_old);
c <=(a and b and c_old) or (a and b_b and c_old) or (a_b and b and c_old) or (a and b and c_b);



end Behavioral;
