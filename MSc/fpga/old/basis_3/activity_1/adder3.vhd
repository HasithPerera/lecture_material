----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/13/2019 10:13:07 PM
-- Design Name: 
-- Module Name: adder3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adder3 is
Port (a0,a1,a2,b0,b1,b2 : in std_logic;
      s0,s1,s2,c2         : out std_logic);
end adder3;

architecture Behavioral of adder3 is

component adder1bit is port
    ( a,c_old,b: in std_logic;
     s,c : out std_logic
    );
end  component;
signal c0,c1,cp: std_logic;

begin
cp <='0';
x0:adder1bit port map(a0,cp,b0,s0,c0);
x1:adder1bit port map(a1,c0,b1,s1,c1);
x2:adder1bit port map(a2,c1,b2,s2,c2);

end Behavioral;
