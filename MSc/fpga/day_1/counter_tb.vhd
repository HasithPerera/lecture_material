--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:34:04 06/13/2020
-- Design Name:   
-- Module Name:   /home/hasith/Documents/Xilinx/day1/counter_tb.vhd
-- Project Name:  day1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: counter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY counter_tb IS
END counter_tb;
 
ARCHITECTURE behavior OF counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT counter
    PORT(
         clock : IN  std_logic;
         clear : IN  std_logic;
         count : IN  std_logic;
         up : IN  std_logic;
         Q : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    
	
	component adder_4bit is port(
		-- a0,a1,a2,a3,a4,b0,b1,b2,b3,b4,c_in: in std_logic;
		A,B : std_logic_vector (3 downto 0);
		c_in : std_logic;
		Z : out std_logic_vector( 3 downto 0);
		c_out : out std_logic);
	end component;
	
	component decoder_3_8 is
	port(
		en 		: in std_logic;
		address 	: in std_logic_vector(2 downto 0); 
		output	: out std_logic_vector(7 downto 0)
	);

	end component;
	
	component ssd_drive is
		port( SW: in std_logic_vector(1 downto 0);
				BTN : in std_logic_vector(1 downto 0);
				SSD : out std_logic_vector(6 downto 0)
		);
	end component;

	
   --Inputs
   signal clock : std_logic := '0';
   signal clear : std_logic := '0';
   signal count : std_logic := '0';
   signal up : std_logic := '0';

	signal SW,BTN: std_logic_vector(1 downto 0);
	signal SSD : std_logic_vector(6 downto 0);
-- 	--uut2 IO
--   signal Q : std_logic_vector(3 downto 0);
--	signal A,B,Z:std_logic_vector(3 downto 0);
--	signal c_in,c_out: std_logic;
	
--	--uut3 io specification
--	signal address: std_logic_vector(2 downto 0);
--	signal en: std_logic;
--	signal output: std_logic_vector(7 downto 0);
	
	--uut4 io specs for sim
	

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
--   uut: counter PORT MAP (
--          clock => clock,
--          clear => clear,
--          count => count,
--          up => up
--        );

--	uut2: adder_4bit port map(Q,B,c_in,Z,c_out);

--	uut3: decoder_3_8 port map(en,address,output);

	uut4: ssd_drive port map(SW,BTN,SSD);
	
   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
		
   end process;
	BTN(0)<=clock;

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		-- check c_in
		
--		B <= "0100";
--		c_in <= '0';
--		clear <='0';count <='0';up<='1';
--		wait for 5 ns;	
--		clear <='1';count <='0';up<='1';
--		wait for 5 ns;	
--		clear <='0';count <='0';up<='1';
--		wait for 5 ns;	
--		clear <='0';count <='1';up<='1';
		--
--		--test for uut3
--		en <= '1';
--		address <="000";
--		wait for 5 ns;
--		address <="001";
--		wait for 5 ns;
--		address <="010";
--		wait for 5 ns;
--		address <="110";
--		wait for 5 ns;
		
		--test for uut4
		
		
		BTN(1) <= '1';
		wait for 5 ns;
		BTN(1) <= '0';
		SW <="11";
			
		
      wait for clock_period*20;
		
      -- insert stimulus here 

--      wait;
   end process;

END;
