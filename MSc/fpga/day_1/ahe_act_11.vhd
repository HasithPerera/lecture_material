----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:59:42 06/20/2020 
-- Design Name: 
-- Module Name:    ahe_act_11 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ahe_act_11 is

port (

	SW : in std_logic_vector(5 downto 0);
	BTN : in std_logic_vector(1 downto 0);
	clock : in std_logic;
	SSD : out std_logic_vector(6 downto 0);
	AN : out std_logic_vector(1 downto 0);
	clk: in std_logic
);

end ahe_act_11;



architecture Behavioral of ahe_act_11 is

component ROM is
port(	Clock   : in std_logic;
	Reset	: in std_logic;	
	Enable	: in std_logic;
	Read	: in std_logic;
	Address	: in std_logic_vector(3 downto 0);
	Data_out: out std_logic_vector(7 downto 0)
);
end component;

component sig_mux is
port(
	data_in : in std_logic_vector (7 downto 0);
	data_out: out std_logic_vector (3 downto 0);
	add : in std_logic	
);
end component;

component bcd is
port (
      bcd : in  std_logic_vector(3 downto 0);            --BCD input
      y   : out std_logic_vector(6 downto 0)       -- 7 bit decoded output.
    );
end component;

	signal D : std_logic_vector(7 downto 0);
	signal Digit: std_logic_vector (3 downto 0);

begin

	rom0 : ROM port map(BTN(0),BTN(1),SW(4),SW(5),SW(3 downto 0),D);
	mux0: sig_mux port map(D,Digit,clk);
	ssd_bcd: bcd port map(Digit,SSD);
	

end Behavioral;

