----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:36:35 06/14/2020 
-- Design Name: 
-- Module Name:    decoder_3_8 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity decoder_3_8 is
port(
	en 		: in std_logic;
	SW 	: in std_logic_vector(2 downto 0); 
	LED	: out std_logic_vector(7 downto 0)
);

end decoder_3_8;

architecture Behavioral of decoder_3_8 is
component decoder_gen is
 generic ( N : integer := 2; M: integer := 4);
 port ( 
  enable: in STD_LOGIC;
  address: in STD_LOGIC_VECTOR (N-1 downto 0);
  pointers: out STD_LOGIC_VECTOR (M-1 downto 0));
end component;

signal temp : std_logic_vector(7 downto 0);

begin

d0: decoder_gen generic map (3,8) port map(en,SW,temp);
LED <= not temp;
end Behavioral;

