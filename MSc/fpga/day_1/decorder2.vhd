
library IEEE ;
use IEEE.std_logic_1164.all;

entity decoder2 is port (
   a, b, e: in std_logic ;
   y: out std_logic_vector (3 downto 0) );
end decoder2 ;

architecture behv of decoder2 is
  signal x : std_logic_vector (2 downto 0);
  begin
     x <= a & b & e;   
     with x select y <=
       "0001" when "001",
       "0010" when "011",
       "0100" when "101",
       "1000" when "111",
       "0000" when others;
end behv;

