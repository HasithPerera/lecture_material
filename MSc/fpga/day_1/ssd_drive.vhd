----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:37:06 06/14/2020 
-- Design Name: 
-- Module Name:    ssd_drive - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ssd_drive is
port( SW: in std_logic_vector(1 downto 0);
		BTN : in std_logic_vector(1 downto 0);
		SSD : out std_logic_vector(6 downto 0)
);
end ssd_drive;

architecture Behavioral of ssd_drive is

component bcd is
port (
      bcd : in  std_logic_vector(3 downto 0);            --BCD input
      y   : out std_logic_vector(6 downto 0)       -- 7 bit decoded output.
		);
end component;

component counter is

port(	clock:	in std_logic;
	clear:	in std_logic;
	count:	in std_logic;
	up:	in std_logic;
	Q:	out std_logic_vector(3 downto 0)
);
end component;

signal bcd_in : std_logic_vector (3 downto 0);

begin

countr0 : counter port map(BTN(0),BTN(1),SW(0),SW(1),bcd_in);
x0: bcd port map(bcd_in,SSD);

end Behavioral;

