----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Hasith Perera(hasith@fos.cmb.ac.lk)
-- 
-- Create Date:    16:49:25 06/20/2020 
-- Design Name: 
-- Module Name:    sig_mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sig_mux is
port(
	data_in : in std_logic_vector (7 downto 0);
	data_out: out std_logic_vector (3 downto 0);
	add : in std_logic

);
end sig_mux;

architecture Behavioral of sig_mux is
	

begin

process(add,data_in)
begin
if (add = '1') then
	data_out <= data_in(3 downto 0);
else
	data_out <= data_in(7 downto 4);
end if;
end process;
--with add select data_out <=
--	data_in(3 downto 0) when '1',
--	data_in(7 downto 4) when '0';
--	--"UUUU" when others;

end Behavioral;

