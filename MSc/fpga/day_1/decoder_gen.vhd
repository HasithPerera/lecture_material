----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:32:53 06/14/2020 
-- Design Name: 
-- Module Name:    decoder_gen - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 

----------------------------------------------
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;
 use IEEE.numeric_std.all;
 use ieee.std_logic_unsigned.all;

 entity decoder_gen is
 generic ( N : integer := 2; M: integer := 4);
 port ( 
  enable: in STD_LOGIC;
  address: in STD_LOGIC_VECTOR (N-1 downto 0);
  pointers: out STD_LOGIC_VECTOR (M-1 downto 0));
 end decoder_gen;

 ---------------------------------------------
 ARCHITECTURE generic_decoder OF decoder_gen IS
 BEGIN
  PROCESS (enable, address)
    VARIABLE temp1 : STD_LOGIC_VECTOR (pointers'HIGH DOWNTO 0);
    VARIABLE temp2 : INTEGER RANGE 0 TO pointers'HIGH;
  BEGIN
    temp1 := (OTHERS => '1');
    temp2 := 0;
    IF (enable='1') THEN
      FOR i IN address'RANGE LOOP -- sel range is 2 downto 0
	IF (address(i)='1') THEN -- Bin-to-Integer conversion
	  temp2:=2*temp2+1;
	ELSE
	  temp2 := 2*temp2;
	END IF;
      END LOOP;
      temp1(temp2):='0';
    END IF;
    pointers <= temp1;
  END PROCESS;
 END generic_decoder;