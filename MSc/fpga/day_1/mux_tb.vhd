--------------------------------------------------------------------------------
-- Company: 
-- Engineer: Hasith Perera(hasith@fos.cmb.ac.lk)
--
-- Create Date:   17:23:13 06/20/2020
-- Design Name:   
-- Module Name:   /home/hasith/Documents/Xilinx/day1/mux_tb.vhd
-- Project Name:  day1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sig_mux

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY mux_tb IS
END mux_tb;
 
ARCHITECTURE behavior OF mux_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sig_mux
    PORT(
         data_in : IN  std_logic_vector(7 downto 0);
         data_out : OUT  std_logic_vector(3 downto 0);
         add : in std_logic
        );
    END COMPONENT;
	 
	 
	component mux_2_to_1 is port( X: in STD_LOGIC_vector(3 downto 0);
										Y: in STD_LOGIC_vector(3 downto 0);
										Z: out STD_LOGIC_vector(3 downto 0);
										clock: in STD_LOGIC);
	end component;

    

   --Inputs
   signal data_in : std_logic_vector(7 downto 0) := (others => '0');
   signal add : std_logic;

 	--Outputs
   signal data_out : std_logic_vector(3 downto 0);

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sig_mux PORT MAP (
          data_in => data_in,
          data_out => data_out,
          add => add
        );
		  
--	uut2:  mux_2_to_1 port map(data_in(3 downto 0),data_in(7 downto 4),data_out,add);

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		data_in <= "00001111";
		add <='1';
      wait for 5 ns;	
		add <='0';
		wait for 5 ns;	
		data_in <= "00101111";
		add <='1';
		wait for 5 ns;	
		add <='0';
      -- insert stimulus here 
		
      wait;
   end process;

END;
