--------------------------------------------------------------
-- 32*8 ROM module (ESD Book Chapter 5)
-- by Weijun Zhang, 04/2001
--
-- ROM model has predefined content for read only purpose
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;		 
use ieee.std_logic_unsigned.all;

entity ROM_13 is
port(	Clock   : in std_logic;
	Reset	: in std_logic;	
	Address	: in std_logic_vector(3 downto 0);
	Data_out: out std_logic_vector(15 downto 0)
);
end ROM_13;

--------------------------------------------------------------

architecture Behav of ROM_13 is

    type ROM_Array is array (0 to 15) 
	of std_logic_vector(15 downto 0);

    constant Content: ROM_Array := (
        0 => "0000000000000011",		-- Suppose ROM has 
        1 => "0000000000000110",		-- prestored value
        2 => "0000000000001100",		-- like this table
        3 => "0000000000011000",
        4 => "0000000000110000",	
        5 => "0000000001100000",
        6 => "0000000011000000",	
        7 => "0000000110000000",
        8 => "0000001100000000",
        9 => "0000011000000000",
       10 => "0000110000000000",
	    11 => "0001100000000000",
       12 => "0011000000000000",
       13 => "0110000000000000",
	    14 => "1100000000000000",
		 15 => "1000000000000001"
	);       

begin
    process(Clock, Reset, Address)
    begin
       if( Reset = '1' ) then
			Data_out <= "ZZZZZZZZZZZZZZZZ";
       elsif( Clock'event and Clock = '1' ) then
			Data_out <= Content(conv_integer(Address));
	    end if;
  
    end process;
end Behav;

--------------------------------------------------------------
