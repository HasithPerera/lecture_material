----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:04:18 06/21/2020 
-- Design Name: 
-- Module Name:    ahe_act_13 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ahe_act_13 is

port(

		clock   	: IN  STD_LOGIC;
		reset 	: in std_logic;
		led 		: OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
		);
end ahe_act_13;

architecture Behavioral of ahe_act_13 is

component counter_n IS
GENERIC ( n : NATURAL := 4 );
PORT (
	clock   : IN  STD_LOGIC;
	reset_n 			: IN  STD_LOGIC;
	Q 					: out std_logic_vector(n-1 downto 0)
);
END component; 

component ROM_13 is
port(	Clock   : in std_logic;
	Reset	: in std_logic;	
	Address	: in std_logic_vector(3 downto 0);
	Data_out: out std_logic_vector(15 downto 0)
);
end component;

signal temp : std_logic_vector(3 downto 0);

begin

	CNT0: counter_n port map(clock,reset,temp);
	ROM0: ROM_13 port map(clock,reset,temp,led);

end Behavioral;

