#!/usr/bin/env python3

import paho.mqtt.client as mqtt
try :
    import Adafruit_BBIO.GPIO as GPIO
except:
    print("Please execute in the beaglbone.\nQuitting")
    exit(-1)
import time

global max_voltage
global adc_bits

global pins

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

def decode_voltage(voltage):
    print('v_dec')
    return int(((2**adc_bits)-1)*(voltage/max_voltage))

def set_voltage(voltage):
    print("Set voltage:{}".format(voltage))
    write_adc(voltage)
    #set digital pins
    
max_voltage = 5.0
adc_bits = 8

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    voltage_cmd=msg.payload.decode('utf-8')
    #try:
    print('raw_decode:{}'.format(voltage_cmd))
    volt_float=float(voltage_cmd)
    print('float_cnvrtd:{}'.format(volt_float))
    if volt_float == None:
        print("Decode_Error")
    else:
        user_input=decode_voltage(volt_float)
        set_voltage(user_input)

def set_io():
    for pin_id in range(11,19):
        pin_name = 'P9_{}'.format(pin_id)
        GPIO.setup(pin_name, GPIO.OUT)
        pins.append('P9_{}'.format(pin_id))

def test_io():
    for pin in pins:
        GPIO.output(pin, GPIO.HIGH)
        time.sleep(0.1)
        GPIO.output(pin, GPIO.LOW)
def write_adc(voltage):
    indx=0
    bin_mod = format(voltage, '#010b')
    print("org:",bin_mod)
    print("mod:",bin_mod[2::])
    for bit in bin_mod[2:10]:
        print(bit)
        if bit == '1':
            #print(indx,pins[indx])
            GPIO.output(pins[indx],GPIO.HIGH)
            indx = indx + 1
        elif bit == '0':
            GPIO.output(pins[indx],GPIO.LOW)
            indx = indx + 1
        

def main():
    
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect("192.168.7.2", 1883, 60)
    client.subscribe("/cmd/set_v")
    try:
        client.loop_forever()
    except:
        print('User Exit command')
        exit(0)


if __name__=="__main__":

    
    pins = [] 
    #pins = ["P9_12",]
    #print(pins)
    set_io()
    test_io()

    max_voltage = 5.0
    adc_bits = 8

    print("Main program exec")
    main()
