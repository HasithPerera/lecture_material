#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import serial

x=[]
y=[]
def main():
    fig,ax = plt.subplots()
    line, = ax.plot([],[],'r')
    ser = serial.Serial('/dev/ttyUSB0',9600)

    def animate(frame_number):
        line_dat=ser.readline()
        in_dat=line_dat[0:-2].decode('utf-8')

        x.append(frame_number)
        y.append(float(in_dat))
        print('{},{}'.format(frame_number,in_dat))
        ax.set_xlim([0,max(x)])
        ax.set_ylim([0,max(y)])
        #plt.xlim([0,max(x)])
        
        line.set_data(x,y)
        print(max(x))
        return line,
    
    ani = animation.FuncAnimation(fig,animate,
            interval = 50,
            blit = False )
    plt.show()

print('Start program')
main()
