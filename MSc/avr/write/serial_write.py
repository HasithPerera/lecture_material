#!/usr/bin/env python3

import serial
import numpy as np

port = '/dev/ttyUSB0'
speed = 9600
time_out = 10


def main():
    print('Start_program')
    ser = serial.Serial(port,speed,timeout=time_out)
    
    #ser.write(''.encode('utf-8'))
    while True:
        try:
            text=input('>')
            ser.write(text.encode('utf-8'))
            print('user:{}'.format(text.encode('utf-8')))
        except:
            print('quit')
            break;


if __name__=='__main__':
    main()
