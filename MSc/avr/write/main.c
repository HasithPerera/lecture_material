#define F_CPU 8000000UL
#define USART_BAUDRATE 9600 
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)
#include <stdio.h>
#include <avr/io.h>


void uart_init(void);
int main(void) {
	char ReceivedByte; 
    uart_init();
	DDRC = 0xff;
	PORTC =0xff;
    while(1) {
		char ReceivedByte;
		while ((UCSRA & (1 << RXC)) == 0) ; 
		ReceivedByte = UDR;
		
		switch (ReceivedByte){
	    case 'a':
				PORTC = 0xff;
				break;
		case 'b':
				PORTC = 0x00;
				break;
		}
		/*while ((UCSRA & (1 << UDRE)) == 0) ; 
		UDR = ReceivedByte;
		
		while ((UCSRA & (1 << UDRE)) == 0) ; 
		UDR = '\r';*/
    }   
    return 0;
}
void uart_init(void) {
	UCSRB |= (1 << RXEN) | (1 << TXEN); 
	UCSRC |= (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1); 
	UBRRH = (BAUD_PRESCALE >> 8); 
	UBRRL = BAUD_PRESCALE;
}
