#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import serial


window = 20
x=np.arange(0,2*window)
y=np.zeros_like(x)

x_final=np.arange(0,window)
y_final=np.zeros_like(x_final)

file_name= 'out.txt'
def data_dump(file_name,data):
    with open(file_name,'a') as infile:
        np.savetxt(infile,data,fmt='%d',delimiter=',',)

def scroll_data(frame,data):
    #indx = frame%window
    indx_2 = frame%(window*2)
    x[indx_2]=frame
    y[indx_2]=data
    if indx_2 == (window*2)-1:
        data_file=np.array([x,y])
        print('File write')
        print(data_file.shape)
        data_dump(file_name,data_file.transpose())


    #print(x)
    if frame<window:
        x_slct=x[0:window]
        y_slct=y[0:window]
    else:
        #print('above 100')
        #x_slct=x[frame:frame]
        val = indx_2-(window-1)
        #print('{},{}'.format(val,indx_2+1))
        if val < 0:
            x_slct=x[val:indx_2+1:-1]
            y_slct=y[val:indx_2+1:-1]
        else:
            x_slct=x[val:indx_2+1]
            y_slct=y[val:indx_2+1]
    return x_slct,y_slct

def main():
    fig,ax = plt.subplots()
    line, = ax.plot([],[],'r')
    ser = serial.Serial('/dev/ttyUSB0',9600)

    def animate(frame_number):
        line_dat=ser.readline()
        in_dat=line_dat[0:-2].decode('utf-8')

        x_final,y_final = scroll_data(frame_number,float(in_dat))
        ax.set_xlim(min(x_final),max(x_final))
        ax.set_ylim([min(y_final),max(y_final)])
        
        line.set_data(x_final,y_final)
        return line,
    
    ani = animation.FuncAnimation(fig,animate,
            interval = 50,
            blit = False )
    plt.show()

print('Start program')
main()
