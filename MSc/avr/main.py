#!/usr/bin/env python3

import serial
import matplotlib.pyplot as plt
import numpy as np


def main():
    print("Start UART")
    with serial.Serial('/dev/ttyUSB0',9600) as ser:
        while True:
            try:
                line_dat = ser.readline()
                input_data=line_dat[0:-2].decode('utf-8')
                print('ser-in:{}'.format(input_data))
            
            except:
                print('Serial open Error')
                break



if __name__=='__main__':
    main()
