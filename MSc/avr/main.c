#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#define BAUD 9600                                   
#define BAUDRATE ((F_CPU)/(BAUD*16UL)-1)           

char command[]="                  ";

unsigned char uart_rx (void);
void uart_tx (unsigned char data);
void uart_init (void);
int uart_putchar_printf(char var, FILE *stream);

static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar_printf, NULL, _FDEV_SETUP_WRITE);

int main(){
	stdout = &mystdout;
	uart_init();
	printf("Start\n");
	uint8_t i = 0;
	while(1){
		printf("%d\n",i=i+1);
		_delay_ms(100);
	}

}


void uart_init (void)
{
    UBRRH = (BAUDRATE>>8);                      
    UBRRL = BAUDRATE;   
    UCSRB|= (1<<TXEN)|(1<<RXEN);               
    UCSRC|= (1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1);  
}

void uart_tx (unsigned char data)
{
    while (!( UCSRA & (1<<UDRE)));               
    UDR = data;                                   
}

unsigned char uart_rx (void)
{
	loop_until_bit_is_set(UCSRA, RXC);            
    return UDR;                                   
}


int uart_putchar_printf(char var, FILE *stream)
{
    if (var == '\n') uart_tx('\r');
    uart_tx(var);
    return 0;
}

