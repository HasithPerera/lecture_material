import paho.mqtt.client as mqtt
import time


client_1 = mqtt.Client()
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("esp32/#")


def on_message(client, userdata, msg):
    user_msg = msg.payload.decode('utf-8')
    with open('mqtt_log.csv',"a+") as fp:

        print("{},{},{}".format(time.time(),msg.topic,msg.payload))
        fp.write("{},{},{},{}\n".format(time.time(),time.strftime("%H:%M:%S,%m-%d-%y"),msg.topic,msg.payload.decode('utf-8')))

client_1.on_connect = on_connect
client_1.on_message = on_message

client_1.connect("localhost", 1883, 60)
client_1.loop_forever()
 
