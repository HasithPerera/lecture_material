pkg load image

img = imread('./Data/eight.tif');
subplot(131)
imshow(img)
title("org img")

subplot(132)

im_ns = im2double(imnoise(img,"salt & pepper",.01));
imshow(im_ns)
title("img ns")


window_len = 1;
processed = zeros(size(img));

for x = 1+window_len:size(img,1)-window_len
  for y =1+window_len:size(img,2)-window_len
    tmp = im_ns(x-window_len:x+window_len,y-window_len:y+window_len);
    %val = 0;%;
    
    processed(x,y)=median(median(tmp));
       
  endfor
endfor

subplot(133)
imshow(processed)
title("img - ns")
