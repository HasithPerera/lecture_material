function [I_o] = rgrw_cnt(I,seed,st)
I_n=zeros(size(I)); %init blank image
I_o=I_n;
for i=1:size(seed,1)
    I_n(seed)=1;
end
q=0;
while(nnz(abs(I_n-I_o)>0))
    I_o=I_n;
    I_n=imdilate(I_n,st);
    tmp=I_n-I_o;%new grown
    loc=find(tmp);
    %check each newly grown with original
    for l=loc'
        if(I(l)==0)
            I_n(l)=0;
        end
    end
    q=q+1;
    
end
fprintf('Region grow %d itterations\n',q)
end

