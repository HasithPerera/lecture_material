function [I_n] = rgrw(I,sd,st)
%RGRW Summary of this function goes here
%   Detailed explanation goes here
I_n=zeros(size(I));
I_o=I_n;
x=sd(:,1);
I_n(sd(1),sd(2))=1; %sd=[x y]
%st=strel('disk',2);
q=0;
while(nnz(abs(I_n-I_o)>0))
    I_o=I_n;
    I_n=imdilate(I_n,st);
    tmp=I_n-I_o;%new grown
    loc=find(tmp);
    for l=loc'
        if(I(l)==0)
            I_n(l)=0;
        end
    end
    q=q+1;
end
fprintf('Region grow %d itterations\n',q)
end

