img  = imread('./Data/NB1ln3.png');

bw = img(:,:,2)<200;

st = strel('Square',1);
cnt = imerode(bw,st);

imagesc(cnt);

I_o = cnt;
I_n = zeros(size(cnt));

%seed pixel
i = find(cnt);

I_n(i)=1;
st=strel('Square',2);

while(nnz(I_n-I_o)>0)
    I_o=I_n;
    I_n=imdilate(I_n,st);
    tmp=I_n-I_o;                %new grown pixels in this iteration
                                
    tmp_loc=find(tmp);
    v(:,:,2)=tmp;
    for j=1:length(tmp_loc)
                                %acceptance criteria - for gray scale
                                %images you need to modify this criteria to
                                %suite the situation
        if  I(tmp_loc(j)) > 0
            I_n(tmp_loc(j))=1;
        else
            I_n(tmp_loc(j))=0;
        end
    end   
end
imagesc(I_n-I_o)