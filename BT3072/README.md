# Image processing - BT3172 (2020)

Source data can be found [here](https://drive.google.com/drive/folders/1VGVjTW1Uz9bjWx-WPKDT0Hm5hD0Q9vZP?usp=sharing)

## Activity 1 (lab work)

Basic arithmatic with images

1. load the `eight.tif` image and do arithmetic operations (+, -, /,*) with constants and look at the histograms
2. load `pillars_of_creation.jpg` and look at histograms of each channel
3. Can you find the differences in the images `puzzel1.bmp` & `puzzel2.bmp`

## Activity 2 (lab work)

Segment the blood cells in the `NB1lm3.png` image and submit the results. [more reading](https://www.sciencedirect.com/science/article/pii/S2352340915001626?via%3Dihub)

## Activity 3

The image files provided in the `ecoli_images` folder contain a set of microscopic images taken in uniform time steps. 

1. Load the images file to matlab
2. Find a suitable threshold value to segment the area occupied by e coli.
3. Plot the variation of area vs time

## Activity 4

Segment gray matter by modifying `region_grow.m` using the `high_res_brain.bmp` image.
