% Author Hasith Perera 
% Region growing - Example code

% load black and white image for processing
I=zeros(100);
I(30:70,30:70)=1;
I(75:80,50:75)=1;

imagesc(I);axis image;
colormap gray
[x y]=ginput(1);
I_n=zeros(size(I));
I_o=I_n;
for i=1:length(x)
    I_n(sub2ind(size(I),fix(y(i)),fix(x(i))))=1;
end

% v image is only to show the internal working as an animation to optimize
% this you can remove this function at a late point.

v=zeros([size(I,1) size(I,2) 3]);
v(:,:,1)=I./max(max(I));
st=strel('Square',3);
 while(nnz(I_n-I_o)>0)
    I_o=I_n;
    I_n=imdilate(I_n,st);
    tmp=I_n-I_o;                %new grown pixels in this iteration
                                
    tmp_loc=find(tmp);
    v(:,:,2)=tmp;
    for j=1:length(tmp_loc)
                                %acceptance criteria - for gray scale
                                %images you need to modify this criteria to
                                %suite the situation
        if  I(tmp_loc(j)) > 0
            I_n(tmp_loc(j))=1;
        else
            I_n(tmp_loc(j))=0;
        end
    end
     v(:,:,3)=I_n;
    imagesc(v);axis image
    drawnow()
    pause(.1)
    
end