 area = []
 for i = 0:20
  file_name = strcat("./Data/ecoli_images/ecoli_TRITC_",num2str(i,"%02d"),'.tif')
  img = imread(file_name);
  %% fast
  bw = img > 210;
  area(i+1) = sum(sum(bw));
  
  %% slow
##  for x = 1:size(img,1)
##    for y = 1:size(img,2)
##      if img(x,y)>210
##        img(x,y) =1;
##      else
##        img(x,y)=0;
##      endif
##    endfor
##  endfor
 % pause(.1)
 % drawnow()
 endfor

plot(area,'o','Linewidth',3)